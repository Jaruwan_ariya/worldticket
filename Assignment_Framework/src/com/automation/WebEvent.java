package com.automation;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.*;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;
import static org.testng.Assert.*;


@SuppressWarnings("deprecation")
public class WebEvent {
	public String projectName = "Projects/TEST/";
	private static final long MAXIMUM_BUSY_WAIT = 300;
	private static final long DEFAULT_WAIT = 15;
	private static final String MAIN_DIRECTORY = (System.getProperty("user.dir").replace("\\", "/")) + "/Framework";
	private static final String DEFAULT_URL = "";
	private static WebDriver executionDriver;
	private By busyIndicator = By.xpath(".//*[@class='subcontainer_loading']/span");
	private String scriptName;
	private String suiteName;
	private String suiteCode;
	private String strURL = "";
	private String strSumStatus = "";						
	private String status;	
	private String suiteModule;
	private String scriptUseTime = "";									
	private String countScript;	
	private String defaultScriptName = "";					
	private String startExecutionScript = "";
	private String endExecutionScript = "";
	public static String browserRunning = "";
	public static String browserVersion = "";
	private int sumScriptUseTime = 0;
	private int countActualImage = 1;
	public static int countErrorList = 0;
	public static int countScriptError = 0;
	public static int countScriptPassed = 0;
	public static int countScriptName = 0;
	public int defaultCountScriptName = 0;
	public int highlightMode = 0;
	public int debugMode = 0;
	private StringBuffer verificationErrors = new StringBuffer();
	public String currentObjectStyle = "";
	public String strTestRunFolder = "";
	public String startExecutionDateTime = "";				
	public String endExecutionDateTime = "";				
	public String browserPrefix = "";
	public String currentErrorList = "";
	public boolean foundErrorOnScript = false;	  
    public static SoftAssert softAssert;

	    
	static Logger log = Logger.getLogger(WebEvent.class);
	
	WebEvent(String suiteCode, String suiteName, String browserType, String URL, String projectName){
		try{
			strURL = URL;
			setURLName(strURL);
			
		}catch(Exception e){
			log.fatal(e);
		}
	}
	
/*
 * *****************************************************************************************************************************
 * Main Controller Function
 * *****************************************************************************************************************************
 **/
	public WebDriver getBrowser(String browser) throws Exception{
		WebDriver executionDriver = null;
		try{
			String defaultBrowser = "FF";
			if(browser == ""){
				browser = defaultBrowser;	
			}
			browserPrefix = browser;
			Capabilities caps;
			switch(browser)
			{
				case "FF":
					//To ignore the SSL certificates
					DesiredCapabilities capabilitiesFF = DesiredCapabilities.firefox();
			        capabilitiesFF.setCapability("firefox.switches", Arrays.asList("--ignore-certificate-errors"));
			        capabilitiesFF.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
					FirefoxProfile ffProfile = new FirefoxProfile();
					ffProfile.setPreference("browser.download.folderList", 2);
					ffProfile.setPreference("browser.download.manager.showWhenStarting", false);
					ffProfile.setPreference("browser.helperApps.neverAsk.saveToDisk","application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/excel, text/csv, text/comma-separated-values, application/csv, application/octet-stream");
					FirefoxBinary ffBinary = new FirefoxBinary();
					ffBinary.setTimeout(java.util.concurrent.TimeUnit.SECONDS.toMillis(90));
					
					executionDriver = new FirefoxDriver(ffBinary, ffProfile, capabilitiesFF);
					
					caps = ((RemoteWebDriver) executionDriver).getCapabilities();
					//browserName = caps.getBrowserName();
					browserVersion = caps.getVersion();					
					
					executionDriver.manage().window().maximize();
					browserRunning = "Firefox";
					break;
				case "IE":				
					File file = new File(MAIN_DIRECTORY + "/SeleniumDriver/IEDriverServer.exe");
					System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
								
					//To bypass security domain
					DesiredCapabilities capabilitiesIE = DesiredCapabilities.internetExplorer();
					capabilitiesIE.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
					capabilitiesIE.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
					capabilitiesIE.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
					executionDriver = new InternetExplorerDriver(capabilitiesIE);
					
					caps = ((RemoteWebDriver) executionDriver).getCapabilities();
					//browserName = caps.getBrowserName();
					browserVersion = caps.getVersion();
					
					executionDriver.manage().window().maximize();
					browserRunning = "IE";
					//processName = "IEDriverServer.exe";

					break;
				case "CH":
					File files = new File(MAIN_DIRECTORY + "/SeleniumDriver/ChromeDriverServer.exe");
					System.setProperty("webdriver.chrome.driver", files.getAbsolutePath());
					
					//To ignore the SSL certificates
					DesiredCapabilities capabilitiesCH = DesiredCapabilities.chrome();
					capabilitiesCH.setCapability("chrome.switches", Arrays.asList("--ignore-certificate-errors"));
					capabilitiesCH.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
					executionDriver = new ChromeDriver(capabilitiesCH);	
					caps = ((RemoteWebDriver) executionDriver).getCapabilities();
					browserVersion = caps.getVersion();
					executionDriver.manage().window().maximize();
					browserRunning = "Chrome";
					break;
				default:
					log.warn("Browser Type does not support.");;
					break;
			}
		}catch(Exception e){
			executionDriver = null;
			log.fatal(e);
			throw new Exception("Error occurred. Can't get browser");
		}
		return executionDriver;	
	}
	
	protected WebElement findElement(final By by){
		try {
			List<WebElement> elements = executionDriver.findElements(by);
			if (elements.size() > 0) {
				return elements.get(0);
			}
			if(highlightMode == 1){
				startHighlightElement(by);
			}
			return null;
		} catch (Exception ex) {
			new WebDriverWait(executionDriver, DEFAULT_WAIT).until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver d) {
					try {
						executionDriver.findElements(by);
						return Boolean.TRUE;
					} catch (Exception ex) {
						return Boolean.FALSE;
					}
				}
			});
		}
		List<WebElement> elements = executionDriver.findElements(by);
		if (elements.size() == 0) {
			return elements.get(0);
		}		
		return null;
	}
	
	private Boolean isElementPresent(WebElement element){
		try {
			return element.getLocation() != null;
		} catch (Exception ex) {
			//ex.printStackTrace();
			return Boolean.FALSE;
		}
	}
	
	protected Boolean isElementDisplayed(WebElement element){
		try {
			return isElementPresent(element) && element.isDisplayed();
		} catch (Exception ex) {
			//ex.printStackTrace();
			return Boolean.FALSE;
		}
	}

	protected Boolean isElementDisplayed(By by){
		try{
			return isElementDisplayed(findElement(by));
		}catch(Exception e){
			//e.printStackTrace();
			return null;
		}
	}
	
	protected Boolean isSpinningWheelDisplayed(){
		try{
			return isElementDisplayed(busyIndicator);
		}catch(Exception e){
			//e.printStackTrace();
			return null;
		}
	}

	protected void waitForSpinningWheelDisplayed(){
		try{
			(new WebDriverWait(executionDriver, 2)).until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver d) {
					return isSpinningWheelDisplayed();
				}
			});
		}catch(Exception e){
			//e.printStackTrace();
		}
	}

	protected void waitForNoSpinningWheelDisplayed(){
		try{
			(new WebDriverWait(executionDriver, MAXIMUM_BUSY_WAIT)).until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver d) {
					return !isSpinningWheelDisplayed();
				}
			});
		}catch(Exception e){
			//e.printStackTrace();
		}
	}
	
	//Wait for busy icon displayed
	void waitForBusyIcon(){
		try {
			(new WebDriverWait(executionDriver, 2)).until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver d) {
					return isBusyIcon();
				}
			});
		} catch (Exception ex) {
			//LOG.debug("wait for busy icon timed out");
		}
	}
	
	//Wait for busy icon disappeared
	protected void waitForNotBusyIcon(){
		(new WebDriverWait(executionDriver, MAXIMUM_BUSY_WAIT)).until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver d) {
				return !isBusyIcon();
			}
		});
	}

	protected Boolean isBusyIcon(){
		return isHourGlassCursorDisplayed() || isSpinningWheelDisplayed();
	}
	
	private Boolean isHourGlassCursorDisplayed(){
		return "wait".equals(executeJavascript("document.body.style.cursor"));
	}
	
	Object executeJavascript(String javascript){
		try {
			Object returnValue = ((JavascriptExecutor) executionDriver).executeScript("return " + javascript);
			return returnValue;
		} catch (Exception ex) {
			return null;
		}
	}
	
	//This function use for wait until the element is presented.
	protected boolean waitForElementPresent(By by) throws Exception{
		boolean elementPresent;
		try{
			WebDriverWait wait = new WebDriverWait(executionDriver, DEFAULT_WAIT);
			wait.until(ExpectedConditions.presenceOfElementLocated(by));
			elementPresent = true;
		}catch(Exception e){
			log.error(e);
			elementPresent = false;
			throw new Exception("Unable to locate WebElement.");
		}
		return elementPresent;
	}
	
	//This function will waiting for object to be enabled. 
	public boolean waitForEnabled(By by) throws Exception{
		boolean elementEnabled;
		try{
			WebDriverWait wait = new WebDriverWait(executionDriver, DEFAULT_WAIT);
			wait.until(ExpectedConditions.elementToBeClickable(by));
			elementEnabled = true;	
		}catch(Exception e){
			log.error(e);
			elementEnabled = false;
			throw new Exception("The element is not enabled.");
		}
		return elementEnabled;
	}
	
//	public String getSuiteCode(){
//		try{
//			return suiteCode;
//		}catch(Exception e){
//			log.error(e);
//			return null;
//		}
//	}
//
//	public void setSuiteCode(String sCode){
//		try{
//			suiteCode = sCode;
//		}catch(Exception e){
//			log.error(e);
//		}
//	}
	
//	public String getSuiteName(){
//		try{
//			return suiteName;
//		}catch(Exception e){
//			log.error(e);
//			return null;
//		}
//	}
//
//	public void setSuiteName(String SuiteName){
//		try{
//			suiteName = SuiteName;
//			log.info("Start executing test suite : " + suiteName);
//			//System.out.println(startExecutionDateTime + " : Start executing test suite : " + suiteName);
//		}catch(Exception e){
//			log.error(e);
//		}
//	}
	
//	public String getScriptName(){
//		try{
//			return scriptName;
//		}catch(Exception e){
//			log.error(e);
//			return null;
//		}
//	}

	public void openBrowser(String BrowserType){
		try{
			executionDriver = getBrowser(BrowserType);
		}catch(Exception e){
			log.error(e);
		}
	}

	// To get the URL name. 
	public String getURLName(){
		try{
			return strURL;
		}catch(Exception e){
			log.error(e);
			return null;
		}
	}
	
	// To set the URL name. 
	public void setURLName(String URLName){
		try{
			if(URLName != ""){
				strURL = URLName;
			}else{
				strURL = DEFAULT_URL;
			}
			Thread.sleep(2000);
		}catch(Exception e){
			log.error(e);
		}
	}
	
	public String getStatus(){
		try{
			return status;
		}catch(Exception e){
			log.error(e);
			return null;
		}
	}

	public void setStatus(String Status){
		try{
			status = Status;
//			System.out.println("Result is " + status);
		}catch(Exception e){
			log.error(e);
		}
	}
	
//	public String getCountScript(){
//		try{
//			return countScript;
//		}catch(Exception e){
//			log.error(e);
//			return null;
//		}
//	}

	public void setCountScript(String CountScript){
		try{
			countScript = CountScript;
		}catch(Exception e){
			log.error(e);
		}
	}
	
//	public String getSuiteModule(){
//		try{
//			return suiteModule;
//		}catch(Exception e){
//			log.error(e);
//			return null;
//		}
//	}
//	
//	public void setSuiteModule(String module){
//		try{
//			suiteModule = module;
//		}catch(Exception e){
//			log.error(e);
//		}
//	}
	
	//------------------------------------------------------------------------------------
	
	protected String getText(By by){
		try{
			WebElement element = executionDriver.findElement(by);		
			return element.getText();
		}catch(Exception e){
			log.error(e);
			return null;
		}			
	}
	
	protected String getValue(By by){
		try{
			WebElement element = executionDriver.findElement(by);		
			return element.getAttribute("value");
		}catch(Exception e){
			log.error(e);
			return null;
		}			
	}
	
//	protected String getId(By by){
//		try{
//			WebElement element = executionDriver.findElement(by);		
//			return element.getAttribute("id");
//		}catch(Exception e){
//			log.error(e);
//			return null;
//		}			
//	}
	
	private void clear(WebElement element){
		try{
			element.sendKeys(Keys.chord(Keys.CONTROL, "a"));
			element.sendKeys(Keys.DELETE);
		}catch(Exception e){
			log.error(e);
		}
	}
	
	public void logError(String FileName, String StepName, String ErrDescription, String ScriptName){
		keepErrorList(FileName, StepName, ErrDescription, ScriptName);
	}
	
	public void keepErrorList(String FileName, String StepName, String ErrDescription, String ScriptName){
		try{
			String input = "";
			
			foundErrorOnScript = true;
			WebEvent.countErrorList = WebEvent.countErrorList + 1;
						
			currentErrorList = StepName + " : " + ErrDescription;;
			//*****************************************************************************
		}catch(Exception e){
			log.error(e);
		}
	}
	
	public void setScriptName(String ScriptName) throws Exception{
		String Status = "";
		try{	
			scriptName = ScriptName;
			countScriptName = countScriptName + 1;
			
			//Set default script name for first script only.
			if(defaultCountScriptName == 0){
				defaultScriptName = scriptName;
				defaultCountScriptName = countScriptName;
			}
			
			//End round of script name.
			if(defaultCountScriptName != countScriptName){
								
				if(foundErrorOnScript == true){
					Status = "Failed";
					countScriptError = countScriptError + 1;
					foundErrorOnScript = false;
				}else{
					Status = "Passed";	
					countScriptPassed = countScriptPassed + 1;
				}
				
				//Start next script.
				defaultScriptName = scriptName;
				defaultCountScriptName = countScriptName;
			}
			
//			if(debugMode == 1){
//				log.info("Executing script : " + scriptName + " (" + countScriptName + " of "  + getCountScript() +" scripts)");
//				//System.out.println(startExecutionScript + " : Executing script : " + scriptName + " (" + countScriptName + " of "  + getCountScript() +" scripts)");
//			}

		}catch(Exception e){
			log.error(e);
			throw new Exception("Error occurred. Please check your script");
		}
	}	
	// Post Run
	protected void Close()throws Exception{
		try{

			String TestStatus = "", latestScriptTestStatus = "";
			if(foundErrorOnScript == true){
				countScriptError = countScriptError + 1;
				foundErrorOnScript = false;
				latestScriptTestStatus = "Failed";
			}else{
				countScriptPassed = countScriptPassed + 1;
				latestScriptTestStatus = "Passed";
			}
			
			//Status of the whole test suite.
			if(countErrorList > 0){
				TestStatus = "Failed";
				System.out.println("The result of scenario is " + TestStatus );
			}else{
				TestStatus = "Passed";	
				System.out.println("The result of scenario is " + TestStatus );
			}
		
			if(TestStatus == "Passed"){
				log.info("Execution Passed!!!");
			}else if(TestStatus == "Failed"){
				log.info("Execution Failed!!!");
			}
						
			//Set to default values
			countErrorList = 0;
			countScriptName = 0;
			countScriptError = 0;
			countScriptPassed = 0;
			browserRunning = "";
			
			executionDriver.quit();
			
		}catch(Exception e){
			log.fatal(e);
		}
	}
	
	/*
	 * *****************************************************************************************************************************
	 * Web page interactive Function
	 * *****************************************************************************************************************************
	 **/
	
	protected boolean Get(String URL) throws Exception{
		boolean Get;
		try{
			executionDriver.get(URL);
			Get = true;
		}catch(Exception e){
			log.fatal(e);
			logError("Failed", "Open website : " + URL, "Unable to open URL : " + URL, scriptName);
			Get = false;
		}
		return Get;
	}
	
	protected void Click(By by, String ObjectName, String ObjectType) throws Exception{
		try{
			WebElement element = executionDriver.findElement(by);

			JavascriptExecutor js = (JavascriptExecutor) executionDriver;
			js.executeScript("arguments[0].click();", element);
			
			waitForSpinningWheelDisplayed();
			waitForBusyIcon();
			waitForNoSpinningWheelDisplayed();
			waitForNotBusyIcon();
		}catch(Exception e){
			log.error(e);
			logError("Failed", "Click on " + ObjectName + " button.", "Error occurred while click on " + ObjectName + " button. Please check.", scriptName);
		}
	}
	
    public String executeJavaScript(String scriptToExecute) throws Exception
    {
    	JavascriptExecutor jsexe = (JavascriptExecutor) executionDriver;
    	String value = null;
        try
        {
        	jsexe.executeScript(scriptToExecute);
            waitForNoSpinningWheelDisplayed();
            waitForNoSpinningWheelDisplayed();
        }
        catch (Exception e)
        {
            log.error(e);
        }
        return value;
    }
	
	protected void Type(final By by, final String Value, String ObjectName, String ObjectType) throws Exception {
		WebElement element = null;
		try{
			waitForEnabled(by);		
			element = executionDriver.findElement(by);
			element.click();
			element.clear();
			
			element = executionDriver.findElement(by);
			element.click();
			element.sendKeys(Value);
					
			element = executionDriver.findElement(by);
			String currentValue = getValue(by);					
			
			element = executionDriver.findElement(by);
			if(element.isEnabled()){
				element.sendKeys(Keys.TAB);
				waitForNotBusyIcon();
				waitForNoSpinningWheelDisplayed();
			}
		}catch (UnhandledAlertException ue){
			log.error(ue);
		}catch(Exception e){
			logError("Failed", "Type in object : " + ObjectName, "Error occurred while type in " + ObjectName + " ,Value = " + Value + ", Please check.", scriptName);
			failedAction(by, Value);
			log.error(e);
		}
		afterAction(by, Value);
	}

    public void Select(By by, String value, String ObjectName, String ObjectType)throws Exception{
    {
    	WebElement tagDiv = null;
        try
        {
            WebElement element = executionDriver.findElement(by);
            JavascriptExecutor js = (JavascriptExecutor) executionDriver;
            if (value.length()>0)
            {
            	if(isElementDisplayed(findElement(By.tagName("ul"))))
                {
            		tagDiv = element.findElement(By.tagName("ul"));
    			}
            	List<WebElement> tagUl = tagDiv.findElements(By.tagName("li"));
                for(int i=0; i<tagUl.size(); i++)
                {
                	List<WebElement> tagSpan = tagUl.get(i).findElements(By.tagName("span"));
                    for(int j=0; j<tagSpan.size(); j++)
                    {
                    	if(tagSpan.get(j).getText().equals(value))
    					{
                    		tagSpan.get(j).click();
    					}
				       	break;
                    }
                    break;
			    }
           }
				waitForSpinningWheelDisplayed();
				waitForNoSpinningWheelDisplayed();
            }catch(Exception e){
                log.error(e);
                logError("Failed", "Click an object : " + ObjectName, "Error occurred while select " + ObjectName + " ,Value = " + value + ", Please check.", scriptName);
                failedAction(by, value);
            }
            afterAction(by, value);
    	}
    }
 
	protected void SelectDDL(final By by, final String Value, String ObjectName, String ObjectType) throws Exception{
		try{
			new WebDriverWait(executionDriver, DEFAULT_WAIT).until(ExpectedConditions.presenceOfElementLocated(by));
			WebElement element = executionDriver.findElement(by);
			if(element.isEnabled()){
				Select select = new Select(element);
				Thread.sleep(1000);
				select.selectByVisibleText(Value);
				waitForSpinningWheelDisplayed();
				waitForNoSpinningWheelDisplayed();
			}else{
				logError("Failed", "Select an object : " + ObjectName, "This object is disabled.", scriptName);
			}
		}catch(Exception e){
			log.error(e);
			logError("Failed", "Select an object : " + ObjectName, "This object is disabled.", scriptName);
            failedAction(by, Value);
        }
        afterAction(by, Value);
	}	
	
	//Verify the text in specific object. 
	protected void verifyValue(By by, String expectedValue, String ObjectName, String ObjectType) throws Exception{
		String objTagName;
		String actualValue = null;
		String result;
		
		try{
			startHighlightElement(by);
			WebElement element = executionDriver.findElement(by);
			objTagName = element.getTagName();
			if(objTagName != null){
				switch(objTagName.toLowerCase()){
				case "input":
					actualValue = element.getAttribute("value").trim();
					break;
				case "textarea":
					actualValue = element.getAttribute("value").trim();
					break;
				case "select":
					Select select = new Select(element);
					WebElement w2 = select.getFirstSelectedOption();
					actualValue = w2.getText().trim();
					break;
				default:
					actualValue = element.getText().trim();
					break;
				}				
			}else{
				actualValue = element.getText().trim();
			}
			
			if(expectedValue.contentEquals(actualValue)){
				result = "Passed";
				}
			else {
				result = "Failed. Expected value =" + expectedValue + " not match with Actual Result = " + actualValue;
				System.out.println("Failed. Expected value =" + expectedValue + " not match with Actual Result = " + actualValue);
				logError("Failed", "Verify value", "Verify value failed. The expected value is not same as actual value. Actual Value = " 
						+ actualValue + ", Expected Value = " + expectedValue, scriptName);
			}
			
			endHighlightElement(by);
		}catch(Error e){
			log.error(e);
			logError("Failed", "Verify value", "An unexpected error occurred while verify value. Please check.", scriptName);
			failedAction(by, expectedValue);

			failedAction(by, expectedValue);
   			throw new Exception("Failed. Not found object to verify value");
		}
		afterAction(by, expectedValue + " (" + result + ")");
	}
	
 /*
 * *****************************************************************************************************************************
 * Miscellaneous Function
 * *****************************************************************************************************************************
 **/
	
	//Get value from object and store in the specified variable. 
	protected String StoreValue(By by, String ObjectName, String ObjectType) throws Exception{
		String storeValue = "";
		String objType;
		try{
			WebElement element = executionDriver.findElement(by);
			objType = element.getAttribute("type");
			if(objType != null){
				if(objType.equalsIgnoreCase("text")){
					storeValue = element.getAttribute("value");
				}else if(objType.contains("select")){
					Select select = new Select(element);
					WebElement w2 = select.getFirstSelectedOption();
					storeValue = w2.getText();
				}else{
					storeValue = element.getText();
				}
			}else{
				storeValue = element.getText();
			}
		}catch(Exception e){
			log.error(e);
			logError("Failed", "StoreValue", "Unable to store value from this object : " + ObjectName + " ,Please check the object or variable.", scriptName);
			failedAction(by, storeValue);
		}
		afterAction(by, storeValue);
		return storeValue;
	}
	
	//To highlight the object. 
	public void startHighlightElement(By by) { 
		try{
			WebElement element = executionDriver.findElement(by);
			//for (int i = 0; i < 2; i++) { 
				JavascriptExecutor js = (JavascriptExecutor) executionDriver;
				//String styleAttribute = (String) js.executeScript("return arguments[0].style", element);
				js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, currentObjectStyle + " border: 3px solid blue;"); 
			//} 
		}catch(Exception e){
			log.error(e);
		}
	}
	
	//To highlight the object. 
	public void endHighlightElement(By by) { 
		try{
			if(isElementPresent(findElement(by))){
				WebElement element = executionDriver.findElement(by);
				//for (int i = 0; i < 2; i++) { 
					JavascriptExecutor js = (JavascriptExecutor) executionDriver; 
					js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, currentObjectStyle); 
				//} 
			}
		}catch(Exception e){
			log.error(e);
		}
	}
	private void afterAction(By by, String inputValue) throws Exception{
		try{
			if(highlightMode == 1){
				if(by != null){
					endHighlightElement(by);
				}
			}
			
		}catch(Exception e){
			log.error(e);
			throw new Exception("Error occurred. Please investigate");
		}
	}
	//The action to do when failed the action. 
	private void failedAction(By by, String inputValue) throws Exception{
		try{
			if(highlightMode == 1){
				if(by != null){
					endHighlightElement(by);
				}
			}
			}catch(Exception e){
			log.error(e);
			throw new Exception("Error occurred. Please investigate");
		}
	}
	

}
