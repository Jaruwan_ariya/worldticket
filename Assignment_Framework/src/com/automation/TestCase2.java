package com.automation;

import org.openqa.selenium.By;
import java.util.Random;

public class TestCase2 {
	protected void Execute(String BrowserType, String URL, String projectName) throws Exception {
		WebEvent Action = new WebEvent("TestCase2", "TestCase2", BrowserType, URL, projectName);
		Action.highlightMode = 1;
		Action.setCountScript("11");
		try {

/*################ Script Name : Navigate_URL ################*/
		// Current execution script
		Action.setScriptName("Navigate_URL");
		
		//Navigate to website.
		Action.openBrowser(BrowserType);
		Action.Get(Action.getURLName());

/*################ Script Name : Select_English_Booking ################*/
		// Current execution script
		Action.setScriptName("Select_English_Booking");
		
		//Click on Lanugage_Switcher
		Action.Click(By.cssSelector("a > label"), "Lanugage_Switcher", "Button");
		//Click on LanguageMenu_English
		Action.Click(By.xpath("//ul[@id='language-sw']/ul/li[2]/a/label"), "LanguageMenu_English", "");

/*################ Script Name : Booking_SearchFlight ################*/
		// Current execution script
		Action.setScriptName("Booking_SearchFlight");
		
		//Click on From
		Action.Click(By.id("airportFromId"), "From", "Textbox");
		//Select "Germany" at SelectCountry_From list box.
		Action.Select(By.xpath(".//*[@class='layout layout_item countries']"), "Germany", "SelectCountry_From", "Link");
		//Select "Bremen (BRE)" at SelectAnAirport_From list box.
		Action.Select(By.xpath(".//*[@class='layout_item airports']"), "Bremen (BRE)", "SelectAnAirport_From", "Link");
		//Select "Spain" at SelectCountry_To list box.
		Action.Select(By.xpath(".//*[@class='countries_list_container active']"), "Spain", "SelectCountry_To", "Link");
		//Select "Palma de Mallorca (PMI)" at SelectAnAirport_To list box.
		Action.Select(By.xpath(".//*[@class='airports_list_container']/div[@class='active']"), "Palma de Mallorca (PMI)", "SelectAnAirport_To", "Link");

/*################ Script Name : VerifyValue_FromTo ################*/
		// Current execution script
		Action.setScriptName("VerifyValue_FromTo");
		
		//Verify value on From, Value = "Bremen (BRE)"
		Action.verifyValue(By.xpath(".//*[@id='airportFromId']"), "Bremen (BRE)", "From", "Textbox");
		//Verify value on To, Value = "Palma de Mallorca (PMI)"
		Action.verifyValue(By.xpath(".//*[@id='airportToId']"), "Palma de Mallorca (PMI)", "To", "Textbox");

/*################ Script Name : BookSeatWithoutSelectDate ################*/
		// Current execution script
		Action.setScriptName("BookSeatWithoutSelectDate");
		
		//Click on BookSeats
		Action.Click(By.xpath(".//*[@class='submit_find_flights']"), "BookSeats", "Button");

/*################ Script Name : Select_DateFromTo ################*/
		// Current execution script
		Action.setScriptName("Select_DateFromTo");
		
		//Click on From_Day3_Row5
		Action.Click(By.xpath(".//*[@class='layout_item date_from']/descendant::div[contains(@data-reactid,'$row_1.$10.0')]"), "From_Day3_Row5", "Label");
		//Click on To_Day4_row5
		Action.Click(By.xpath(".//*[@class='layout_item date_to']/descendant::div[contains(@data-reactid,'$row_1.$11.0')]"), "To_Day4_row5", "Label");
		//Click on BookSeats
		Action.Click(By.xpath(".//*[@class='submit_find_flights']"), "BookSeats", "Button");

/*################ Script Name : Verify_BookingDetail ################*/
		// Current execution script
		Action.setScriptName("Verify_BookingDetail");
		
		//Verify value on Go_Dep_AirportCode, Value = "BRE"
		Action.verifyValue(By.xpath("(.//*[@class='airport_name'])[1]/descendant::span[3]"), "BRE", "Go_Dep_AirportCode", "Label");
		//Verify value on Go_Arr_AirportCode, Value = "PMI"
		Action.verifyValue(By.xpath("(.//*[@class='airport_name'])[2]/descendant::span[3]"), "PMI", "Go_Arr_AirportCode", "Label");
		//Verify value on Go_Date, Value = "Fr. 10 Feb"
		Action.verifyValue(By.xpath("(.//*[@class='flight_search_page_item'])[1]/descendant::div[@class='label'][1]"), "Fr. 10 Feb", "Go_Date", "Label");
		//Verify value on Go_FlightNumber, Value = "ST4570"
		Action.verifyValue(By.xpath("(.//*[@class='flight_table'])[1]/descendant::tr[2]/td[2]/div"), "ST4570", "Go_FlightNumber", "Label");
		//Store value from Go_FlightNumber into FlightNo1 variable.
		String FlightNo1 = Action.StoreValue(By.xpath("(.//*[@class='flight_table'])[1]/descendant::tr[2]/td[2]/div"), "Go_FlightNumber", "Label");
		//Verify value on Return_Dep_AirportCode, Value = "PMI"
		Action.verifyValue(By.xpath("(.//*[@class='airport_name'])[3]/descendant::span[3]"), "PMI", "Return_Dep_AirportCode", "Label");
		//Verify value on Return_Arr_AirportCode, Value = "BRE"
		Action.verifyValue(By.xpath("(.//*[@class='airport_name'])[4]/descendant::span[3]"), "BRE", "Return_Arr_AirportCode", "Label");
		//Verify value on Return_Date, Value = "Sa. 11 Feb"
		Action.verifyValue(By.xpath("(.//*[@class='flight_search_page_item'])[2]/descendant::div[@class='label'][1]"), "Sa. 11 Feb", "Return_Date", "Label");
		//Verify value on Return_FlightNumber, Value = "ST4571"
		Action.verifyValue(By.xpath("(.//*[@class='flight_table'])[2]/descendant::tr[2]/td[2]/div"), "ST4571", "Return_FlightNumber", "Label");
		//Store value from Return_FlightNumber into FlightNo2 variable.
		String FlightNo2 = Action.StoreValue(By.xpath("(.//*[@class='flight_table'])[2]/descendant::tr[2]/td[2]/div"), "Return_FlightNumber", "Label");

/*################ Script Name : Select_Flight ################*/
		// Current execution script
		Action.setScriptName("Select_Flight");
		
		//Click on Go_SelectFlight
		Action.Click(By.xpath("(//td[@class='flight_table_cell']/div[@class='table_select']/label)[1]"), "Go_SelectFlight", "RadioButton");
		//Verify value on Go_YourSelection, Value = "10.02.17\nBremen (BRE)\nPalma de Mallorca (PMI)\n06:00 - 08:30\nST4570"
		Action.verifyValue(By.xpath("(.//*[@class='your_choice_item']/descendant::div)[1]"), "10.02.17\nBremen (BRE)\nPalma de Mallorca (PMI)\n06:00 - 08:30\nST4570", "Go_YourSelection", "Label");
		//Click on Return_SelectFlight
		Action.Click(By.xpath("(//td[@class='flight_table_cell']/div[@class='table_select']/label)[2]"), "Return_SelectFlight", "RadioButton");
		//Verify value on Return_YourSelection, Value = "11.02.17\nPalma de Mallorca (PMI)\nBremen (BRE)\n09:15 - 11:55\nST4571"
		Action.verifyValue(By.xpath("(.//*[@class='your_choice_item is_to_item']/descendant::div)[1]"), "11.02.17\nPalma de Mallorca (PMI)\nBremen (BRE)\n09:15 - 11:55\nST4571", "Return_YourSelection", "Label");
		//Click on Next
		Action.Click(By.xpath(".//button[text()='Next']"), "Next", "Button");

/*################ Script Name : Input_Passenger ################*/
		// Current execution script
		Action.setScriptName("Input_Passenger");

		//Select "Mr." at Title list box.
		Action.SelectDDL(By.xpath(".//select[contains(@class,'select_item')]"), "Mr.", "Title", "Dropdownlist");
		//Enter "Steve" to FirstName field.
		Action.Type(By.xpath("(.//*[@class='passenger_item'])[1]/div/input[@class='input_item']"), "Steve", "FirstName", "Textbox");
		//Enter "Jobs" to Lastname field.
		Action.Type(By.xpath("(.//*[@class='passenger_item'])[2]/div/input[@class='input_item']"), "Jobs", "Lastname", "Textbox");
		//Verify value on Go_YourSelection, Value = "10.02.17\nBremen (BRE)\nPalma de Mallorca (PMI)\n06:00 - 08:30\nST4570\nPassengers\nMr. Steve Jobs"
		Action.verifyValue(By.xpath("(.//*[@class='your_choice_item']/descendant::div)[1]"), "10.02.17\nBremen (BRE)\nPalma de Mallorca (PMI)\n06:00 - 08:30\nST4570\nPassengers\nMr. Steve Jobs", "Go_YourSelection", "Label");
		//Verify value on Return_YourSelection, Value = "11.02.17\nPalma de Mallorca (PMI)\nBremen (BRE)\n09:15 - 11:55\nST4571\nPassengers\nMr. Steve Jobs"
		Action.verifyValue(By.xpath("(.//*[@class='your_choice_item is_to_item']/descendant::div)[1]"), "11.02.17\nPalma de Mallorca (PMI)\nBremen (BRE)\n09:15 - 11:55\nST4571\nPassengers\nMr. Steve Jobs", "Return_YourSelection", "Label");
		//Click on Next
		Action.Click(By.xpath(".//button[text()='Next']"), "Next", "Button");

/*################ Script Name : SelectedSeat ################*/
		// Current execution script
		Action.setScriptName("SelectedSeat");
		
		//Click on Go_Seat_21D
		Action.Click(By.xpath("(.//table[@id='seat-map-table'])[1]/tbody/tr[4]/td[20]/a"), "Go_Seat_21D", "Image");
		//Verify value on Go_SelectedSeat, Value = "21D"
		Action.verifyValue(By.xpath("(.//table[@class='seatmap-passengers-info-to'])[1]/tbody/tr/td[2]"), "21D", "Go_SelectedSeat", "Label");
		//Verify value on Go_YourSelection, Value = "10.02.17\nBremen (BRE)\nPalma de Mallorca (PMI)\n06:00 - 08:30\nST4570\nPassengers\nSelected Seat\nMr. Steve Jobs\n21D"
		Action.verifyValue(By.xpath("(.//*[@class='your_choice_item']/descendant::div)[1]"), "10.02.17\nBremen (BRE)\nPalma de Mallorca (PMI)\n06:00 - 08:30\nST4570\nPassengers\nSelected Seat\nMr. Steve Jobs\n21D", "Go_YourSelection", "Label");
		//Verify value on Go_SelectedSeat_YourSelection, Value = "21D"
		Action.verifyValue(By.xpath("(.//*[@class='passengers_choice'])[1]/div[4]"), "21D", "Go_SelectedSeat_YourSelection", "Label");
		//Click on Return_Seat_21F
		Action.Click(By.xpath("(.//table[@id='seat-map-table'])[2]/tbody/tr[2]/td[20]/a"), "Return_Seat_21F", "Image");
		//Verify value on Return_SelectedSeat, Value = "21F"
		Action.verifyValue(By.xpath("(.//table[@class='seatmap-passengers-info-to'])[2]/tbody/tr/td[2]"), "21F", "Return_SelectedSeat", "Label");
		//Verify value on Return_YourSelection, Value = "	\n11.02.17\nPalma de Mallorca (PMI)\nBremen (BRE)\n09:15 - 11:55\nST4571\nPassengers\nSelected Seat\nMr. Steve Jobs\n21F"
		Action.verifyValue(By.xpath("(.//*[@class='your_choice_item is_to_item']/descendant::div)[1]"), "11.02.17\nPalma de Mallorca (PMI)\nBremen (BRE)\n09:15 - 11:55\nST4571\nPassengers\nSelected Seat\nMr. Steve Jobs\n21F", "Return_YourSelection", "Label");
		//Verify value on Return_SelectedSeat_YourSelection, Value = "21F"
		Action.verifyValue(By.xpath("(.//*[@class='passengers_choice'])[2]/div[4]"), "21F", "Return_SelectedSeat_YourSelection", "Label");
		//Click on Next
		Action.Click(By.xpath(".//button[text()='Next']"), "Next", "Button");

/*################ Script Name : Verify_InfoBeforePayment ################*/
		// Current execution script
		Action.setScriptName("Verify_InfoBeforePayment");
		
		//Verify value on Go_DepartureTime, Value = "10.02.17"
		Action.verifyValue(By.xpath("(.//*[@class='departure-time'])[1]"), "10.02.17", "Go_DepartureTime", "Label");
		//Verify value on Go_Dep_AirportCode, Value = "BRE"
		Action.verifyValue(By.xpath("(.//*[@class='booking_details_flight_info']/descendant::div[@class='airport_name'])[1]/span[3]"), "BRE", "Go_Dep_AirportCode", "Label");
		//Verify value on Go_Arr_AirportCode, Value = "PMI"
		Action.verifyValue(By.xpath("(.//*[@class='booking_details_flight_info']/descendant::div[@class='airport_name'])[2]/span[3]"), "PMI", "Go_Arr_AirportCode", "Label");
		//Verify value on Go_FlightNumber, Value = FlightNo1
		Action.verifyValue(By.xpath("(.//*[@class='booking_details_flight_info']/descendant::div[@class='flight_route route'])[1]/span[2]"), FlightNo1, "Go_FlightNumber", "Label");
		//Verify value on Go_PassengerName, Value = "Mr. Steve Jobs"
		Action.verifyValue(By.xpath("(.//*[@class='passenger_name'])[1]"), "Mr. Steve Jobs", "Go_PassengerName", "Label");
		//Verify value on Go_SeatNumber, Value = "Seat number 21D"
		Action.verifyValue(By.xpath("(.//*[@class='seat_number'])[1]"), "Seat number 21D", "Go_SeatNumber", "Label");
		//Verify value on Go_Amount, Value = "10,00 EUR"
		Action.verifyValue(By.xpath("(.//*[@class='amount'])[1]"), "10,00 EUR", "Go_Amount", "Label");
		//Verify value on Go_YourSelection, Value = "10.02.17\nBremen (BRE)\nPalma de Mallorca (PMI)\n06:00 - 08:30\nST4570\nPassengers\nSelected Seat\nMr. Steve Jobs\n23D"
		Action.verifyValue(By.xpath("(.//*[@class='flight_choice_label'])[1]"), "10.02.17\nBremen (BRE)\nPalma de Mallorca (PMI)\n06:00 - 08:30\nST4570\nPassengers\nSelected Seat\nMr. Steve Jobs\n21D", "Go_YourSelection", "Label");
		//Verify value on Return_DepartureTime, Value = "11.02.17"
		Action.verifyValue(By.xpath("(.//*[@class='departure-time'])[2]"), "11.02.17", "Return_DepartureTime", "Label");
		//Verify value on Return_Dep_AirportCode, Value = "PMI"
		Action.verifyValue(By.xpath("(.//*[@class='booking_details_flight_info to']/descendant::div[@class='airport_name'])[1]/span[3]"), "PMI", "Return_Dep_AirportCode", "Label");
		//Verify value on Return_Arr_AirportCode, Value = "BRE"
		Action.verifyValue(By.xpath("(.//*[@class='booking_details_flight_info to']/descendant::div[@class='airport_name'])[2]/span[3]"), "BRE", "Return_Arr_AirportCode", "Label");
		//Verify value on Return_FlightNumber, Value = FlightNo2
		Action.verifyValue(By.xpath("(.//*[@class='booking_details_flight_info to']/descendant::div[@class='flight_route'])[1]/span[2]"), FlightNo2, "Return_FlightNumber", "Label");
		//Verify value on Return_Passenger, Value = "Mr. Steve Jobs"
		Action.verifyValue(By.xpath("(.//*[@class='passenger_name'])[2]"), "Mr. Steve Jobs", "Return_Passenger", "Label");
		//Verify value on Return_SeatNumber, Value = "Seat number 21F"
		Action.verifyValue(By.xpath("(.//*[@class='seat_number'])[2]"), "Seat number 21F", "Return_SeatNumber", "Label");
		//Verify value on Return_Amount, Value = "10,00 EUR"
		Action.verifyValue(By.xpath("(.//*[@class='amount'])[2]"), "10,00 EUR", "Return_Amount", "Label");
		//Verify value on Return_YourSelection, Value = "11.02.17\nPalma de Mallorca (PMI)\nBremen (BRE)\n09:15 - 11:55\nST4571\nPassengers\nSelected Seat\nMr. Steve Jobs\n21F"
		Action.verifyValue(By.xpath("(.//*[@class='flight_choice_label'])[2]"), "11.02.17\nPalma de Mallorca (PMI)\nBremen (BRE)\n09:15 - 11:55\nST4571\nPassengers\nSelected Seat\nMr. Steve Jobs\n21F", "Return_YourSelection", "Label");
		//Verify value on TotalAmount, Value = "20,00 EUR"
		Action.verifyValue(By.xpath(".//*[@class='flight_route amount']/div[2]"), "20,00 EUR", "TotalAmount", "Label");
		//Verify value on Total_YourSelection_Name, Value = "	\n1. Mr. Steve Jobs\n20,00"
		Action.verifyValue(By.xpath(".//*[@class='your_choice']/child::div[5]/div[2]"), "1. Mr. Steve Jobs\n20,00", "Total_YourSelection_Name", "Label");
		//Verify value on Total_Fee, Value = "Total fee\n20,00"
		Action.verifyValue(By.xpath(".//*[@class='total_fee']/div[@class='total_fee_amount']"), "Total fee\n20,00", "Total_Fee", "Label");

			Action.setStatus("Passed");
			// Close browser
			Action.Close();
		}
		catch (Exception ex) {
			System.out.print(ex.getMessage());
			Action.setStatus("Failed");
			// Close browser
			Action.Close();
		}

	}
}

