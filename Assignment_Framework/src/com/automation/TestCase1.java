package com.automation;

import org.openqa.selenium.By;
import java.util.Random;

public class TestCase1 {
	protected void Execute(String BrowserType, String URL, String projectName) throws Exception {
		WebEvent Action = new WebEvent("TestCase1", "TestCase1", BrowserType, URL, projectName);
		Action.highlightMode = 1;
		Action.setCountScript("3");
	
		try {
			
			// Current execution script
			Action.setScriptName("Navigate_URL");
			
			Action.openBrowser(BrowserType);
			Action.Get(Action.getURLName());
			
			// Current execution script
			Action.setScriptName("Verify_GermanLanguage");
			
			Action.verifyValue(By.xpath(".//*[contains(@class,'language')]/div/a/label"), "Deutsch", "languageMenu", "DDL");
			Action.verifyValue(By.xpath(".//*[@class='site-nav site-nav--desk js-site-nav']/ul/li[1]/a/span"), "Buchen", "Menu", "Label");
			Action.verifyValue(By.xpath(".//*[@class='site-nav site-nav--desk js-site-nav']/ul/li[2]/a/span"), "Planen", "Menu", "Label");
			Action.verifyValue(By.xpath(".//*[@class='site-nav site-nav--desk js-site-nav']/ul/li[3]/a/span"), "Fliegen", "Menu", "Label");
			Action.verifyValue(By.xpath(".//*[@class='site-nav site-nav--desk js-site-nav']/ul/li[4]/a/span"), "Reiseführer", "Menu", "Label");

			// Current execution script
			Action.setScriptName("Verify_EnglishLanguage");
			
			//Change language from German to English
			Action.Click(By.cssSelector("a > label"), "LanguageMenu", "");
			Action.Click(By.xpath("//ul[@id='language-sw']/ul/li[2]/a/label"), "LanguageMenu", "");

			//Verify value on Language_Label, Value = "English"
			Action.verifyValue(By.xpath(".//*[contains(@class,'language')]/div/a/label"), "English", "Language_Label", "Label");
			//Verify value on Menu_Booking, Value = "Booking"
			Action.verifyValue(By.xpath(".//*[@class='site-nav site-nav--desk js-site-nav']/ul/li[1]/a/span"), "Booking", "Menu_Booking", "Button");
			//Verify value on Menu_Planing, Value = "Planing"
			Action.verifyValue(By.xpath(".//*[@class='site-nav site-nav--desk js-site-nav']/ul/li[2]/a/span"), "Planning", "Menu_Planing", "Button");
			//Verify value on Menu_Flying, Value = "Flying"
			Action.verifyValue(By.xpath(".//*[@class='site-nav site-nav--desk js-site-nav']/ul/li[3]/a/span"), "Flying", "Menu_Flying", "Button");
			//Verify value on Menu_TravelGuide, Value = "Travel guide"
			Action.verifyValue(By.xpath(".//*[@class='site-nav site-nav--desk js-site-nav']/ul/li[4]/a/span"), "Travel guide", "Menu_TravelGuide", "Button");

			Action.setStatus("Passed");
			// Close browser
			Action.Close();
		}
		catch (Exception ex) {
			System.out.print(ex.getMessage());
			Action.setStatus("Failed");
			// Close browser
			Action.Close();
		}

	}
}

