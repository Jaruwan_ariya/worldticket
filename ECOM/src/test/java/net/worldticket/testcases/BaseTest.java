package net.worldticket.testcases;

/**
 * Created by jaruwan.a on 20/03/2017.
 */
import net.worldticket.test.library.*;
import net.worldticket.test.pages.*;

import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestContext;
import org.testng.TestRunner;
import org.testng.annotations.*;

public class BaseTest {

    public Framework framework = new Framework();
    public PageInteraction page = new PageInteraction();
    
    
    Logger log = LoggerFactory.getLogger(BaseTest.class);

    public BaseTest() {

    }
    
    @BeforeSuite
    //Create test suite folder
    public void setupTestSuite(ITestContext context) {
        try {

            TestRunner runner = (TestRunner) context;
            runner.setOutputDirectory(framework.globalTestRunFolder);

            log.info("Establishing test output directory at " + framework.globalTestRunFolder);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
