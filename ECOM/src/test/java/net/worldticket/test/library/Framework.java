package net.worldticket.test.library;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.asserts.SoftAssert;
import ru.yandex.qatools.allure.annotations.Step;

import java.io.File;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * Created by jaruwan.a on 27/03/2017.
 */
public class Framework {

    public static WebDriver driver;
    public static int DEFAULT_WAIT = 90;
    public static String globalTestRunFolder;
    public static SoftAssert softAssert;

    Logger log = LoggerFactory.getLogger(Framework.class);

    public Framework() {

    }

    // To start the web driver.
    @Step("Starting {0} driver.")
    public void startDriver(String browser) {
        try {
            switch(browser.toLowerCase()) {
                case "chrome":
                	
                    File files = new File(System.getProperty("user.dir") + "/drivers/" + "chromedriver.exe");
                    System.setProperty("webdriver.chrome.driver", files.getAbsolutePath());

                    //To ignore the SSL certificates
                    DesiredCapabilities capabilitiesCH = DesiredCapabilities.chrome();
                    capabilitiesCH.setCapability("chrome.switches", Arrays.asList("--ignore-certificate-errors"));
                    capabilitiesCH.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);

                    driver = new ChromeDriver(capabilitiesCH);
                    driver.manage().timeouts().implicitlyWait(DEFAULT_WAIT, TimeUnit.SECONDS);

                    Dimension d = new Dimension(1280, 720);
                    driver.manage().window().setSize(d);
                    driver.manage().window().maximize();

                    log.info("Starting driver with chrome browser.");

                    break;
                case "firefox":
                    //To ignore the SSL certificates
                    DesiredCapabilities capabilitiesFF = DesiredCapabilities.firefox();
                    capabilitiesFF.setCapability("firefox.switches", Arrays.asList("--ignore-certificate-errors"));
                    capabilitiesFF.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);

                    FirefoxProfile ffProfile = new FirefoxProfile();
                    ffProfile.setPreference("browser.download.folderList", 2);
                    ffProfile.setPreference("browser.download.manager.showWhenStarting", false);

                    ffProfile.setPreference("browser.helperApps.neverAsk.saveToDisk","application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/excel, text/csv, text/comma-separated-values, application/csv, application/octet-stream");

                    FirefoxBinary ffBinary = new FirefoxBinary();
                    ffBinary.setTimeout(java.util.concurrent.TimeUnit.SECONDS.toMillis(DEFAULT_WAIT));

                    driver = new FirefoxDriver(ffBinary, ffProfile, capabilitiesFF);
                    driver.manage().window().maximize();

                    log.info("Starting driver with firefox browser.");
                    break;
                default:
                    log.warn("No browser specified.");
                    break;
            }
        } catch (Exception ex) {
            log.error("Java exception occurred : ", ex);
        }
    }

    //To stop webdriver.
    @Step("Ending driver.")
    public void stopDriver() {
        try {
            driver.quit();
            log.info("End driver.");
        } catch (Exception ex) {
            log.error("Java exception occurred : ", ex);
        }
    }

    //Soft Assert checkpoint.
    public void assertAll() {
        if (softAssert != null) {
            softAssert.assertAll();
            softAssert = null;
        }
    }
}