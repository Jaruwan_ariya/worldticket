package net.worldticket.test.steps;

import net.worldticket.test.pages.*;
import net.worldticket.testcases.*;
import net.worldticket.test.library.*;

import org.openqa.selenium.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;

import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;


/**
 * Created by jaruwan.a on 07/04/2017.
 */
@Features("ECOM")
@Stories({"wts-5 : User can create new account"})
public class CreateNewAccount extends BaseTest {

    @Parameters("browser")
    @BeforeClass(description = "Start extent report.")
    public void setupTest(@Optional("chrome") String browser) {
        framework.startDriver(browser);
    }

    
    @Parameters("url")
    @Test(priority = 1, description = "User Open URL and change to English language")
    public void OpenUrlAndChangelanguage(String url) throws Exception {	   
    	
    	page.openURL(url);
    	
	   	if (!page.GetText(HomePage.EngMenu).contains("English"))
	       {
	       	page.click(HomePage.EngMenu);    
	       	page.lockExpandedMenu(HomePage.ExpandLanguageMenu);
	       	page.click(HomePage.languageEnglish);
	       	Thread.sleep(2000);
	       }
    }
    
    @Test(priority = 2, description = "User can create new account")
    public void CreateNewAccount() throws Exception {	   	
		page.click(HomePage.login);
		page.click(LoginPage.btnCreateAccount);
		page.verifyValue(LoginPage.labCreateAccount, "Create an Account");
		page.type(LoginPage.txtEmailAddress,page.GenerateRandomCode("5") + "@worldticket.net");
		page.type(LoginPage.txtPassword, "password");
		page.type(LoginPage.txtConfirmPassword, "password");
		page.type(LoginPage.ddlTitle, "Ms.");
		page.type(LoginPage.txtFirstname, "auto_firstname");
		page.type(LoginPage.txtLastname, "auto_lastname");
		page.click(LoginPage.btnSubmit);
		
			
		page.verifyValue(HomePage.logOut, "Log Out");
		page.verifyValue(LoginPage.successMsg, "Thank you for registering with DAT Danish Air Transport.");
		
		page.click(HomePage.logOut);
        
    }
    
    @AfterClass(description = "Stop test driver.")
    public void endingTest() {
        framework.stopDriver();
    }
}