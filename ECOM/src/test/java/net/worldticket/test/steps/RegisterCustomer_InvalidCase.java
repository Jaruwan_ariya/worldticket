package net.worldticket.test.steps;

import net.worldticket.test.pages.*;
import net.worldticket.testcases.*;
import net.worldticket.test.library.*;

import org.openqa.selenium.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;


/**
 * Created by jaruwan.a on 20/03/2017.
 */
@Features("ECOM")
@Stories({"wts-1.1 : User login with invalid email or password"})
public class RegisterCustomer_InvalidCase extends BaseTest {

    @Parameters("browser")
    @BeforeClass(description = "Start extent report.")
    public void setupTest(@Optional("chrome") String browser) {
        framework.startDriver(browser);
    }

    @Parameters("url")
    @Test(priority = 1, description = "User Open URL and change to English language")
    public void OpenUrlAndChangelanguage(String url) throws Exception {	   
    	
    	page.openURL(url);
    	
	   	if (!page.GetText(HomePage.EngMenu).contains("English"))
	       {
	       	page.click(HomePage.EngMenu);    
	       	page.lockExpandedMenu(HomePage.ExpandLanguageMenu);
	       	page.click(HomePage.languageEnglish);
	       	Thread.sleep(2000);
	       }
    }   

    @Test(priority = 2, description = "user login with invalid email and password.")
    public void RegisterCustomerInvalid() throws Exception {
    	
        page.click(HomePage.login);
        page.verifyValue(LoginPage.labelHaveAcct, "If you have an account with us, please log in.");
        page.type(LoginPage.emailAddress,"aaaa@worldticket.net");
        page.type(LoginPage.password, "R@m@1234");
        page.click(LoginPage.loginRegister);
        
        page.verifyValue(LoginPage.errorMsg, "Invalid login or password.");
        
    }
    
    @AfterClass(description = "Stop test driver.")
    public void endingTest() {
        framework.stopDriver();
    }
}