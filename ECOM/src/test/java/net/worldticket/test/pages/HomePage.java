package net.worldticket.test.pages;

import org.openqa.selenium.By;

/**
 * Created by jaruwan.a on 20/03/2017 AD.
 */
public class HomePage {

    //Language & Currency
    public static By EngMenu = By.xpath("//div[@id='lang-switcher-wrapper-regular']/div/a/span/span[2]");
    public static By DenMenu = By.xpath("//div[@id='lang-switcher-wrapper-regular']/div/a/span/span[3]");
    public static By ExpandLanguageMenu = By.xpath(".//*[@id='lang-switcher-wrapper-regular']/div/ul");
    public static By languageEnglish = By.xpath(".//*[@id='lang-switcher-wrapper-regular']/div/ul/li[2]/a");

    //Main Menu
    public static By datLogo = By.xpath(".//*[@class='logo logo--regular']/img");
    
    
    //Account
    public static By login = By.xpath(".//*[@id='header-account']/ul/li[2]/a");
    public static By logOut = By.xpath(".//*[@title='Log Out']");



}

