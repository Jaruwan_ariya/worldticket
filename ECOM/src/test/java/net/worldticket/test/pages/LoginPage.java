package net.worldticket.test.pages;

import org.openqa.selenium.By;


/**
 * Created by jaruwan.a on 20/03/2017.
 */
public class LoginPage {
    

	//Registered Customers
	public static By labelHaveAcct = By.xpath(".//*[@id='login-form']/div[1]/p[1]");
	public static By emailAddress = By.id("email");
	public static By password = By.id("pass");
	public static By loginRegister = By.xpath(".//*[@id='send2']");
	public static By forgotPassword = By.xpath(".//*[@id='login-form']/div[2]/a");
	public static By errorMsg = By.xpath(".//*[@class='error-msg']/ul/li[1]");
	public static By successMsg = By.xpath(".//*[@class='dashboard']/ul");
	
	//Create Account
	public static By btnCreateAccount = By.xpath(".//*[@title='Create an Account']");
	public static By labCreateAccount = By.xpath(".//*[@class='account-create']/div");
	public static By txtEmailAddress = By.id("email_address");
	public static By txtPassword = By.id("password");
	public static By txtConfirmPassword = By.id("confirmation");	
	public static By ddlTitle = By.id("prefix");
	public static By txtFirstname = By.id("firstname");
	public static By txtMiddlename = By.id("middlename");
	public static By txtLastname = By.id("lastname");
	public static By btnSubmit = By.xpath(".//*[@class='buttons-set']/descendant::button");
	
    
}

