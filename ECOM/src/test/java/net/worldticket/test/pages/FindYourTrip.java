package net.worldticket.test.pages;

import org.openqa.selenium.By;

/**
 * Created by jaruwan.a on 20/03/2017.
 */
public class FindYourTrip {

    //Booking Tab
    public static By bookingMenu = By.xpath("//ul[@id='nav']/li[5]/a/span");
    public static By traveltypeReturn = By.id("traveltype-return");
    public static By traveltypeOneWay = By.id("traveltype-one-way");
    public static By origin = By.xpath(".//input[@id='origin']");
    public static By destination = By.xpath(".//input[@id='destination']");
    public static By deptDate = By.xpath("(.//input[@id='departdate'])");
    public static By returnDate = By.id("returndate");
    public static By date1st = By.xpath("(//a[contains(text(),'1')])[0]");
    public static By date13th = By.xpath("(//a[contains(text(),'13')])[3]");
    
    
    
    
    public static By searchBtn = By.id("btnSearch");
    
    
}

